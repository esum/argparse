#pragma once

#include <cstring>
#include <optional>


namespace arg {

template <typename T, typename... Args>
inline constexpr std::optional<T> get_arg(int argc, char const **argv, char const *argname, Args... argnames)
{
	/// Get the first value of argument if it exists

	auto res = std::find_if(argv, argv + argc, [&](auto& arg) { return strcmp(arg, argname) == 0; });
	res++;
	if (res >= argv + argc) {
		if constexpr (sizeof...(Args))
			return get_arg<T>(argc, argv, argnames...);
		else
			return { };
	}
	else {
		if constexpr (std::is_same_v<T, int>)
			return std::atoi(*res);
		else if constexpr (std::is_same_v<T, long>)
			return std::atol(*res);
		else if constexpr (std::is_same_v<T, long long>)
			return std::atoll(*res);
		else
			return *res;
	}

	return { };
}

template <typename T, int n, char prefix = '-'>
inline constexpr std::pair<int, std::optional<T>> get_positional(int argc, char const **argv, std::initializer_list<char const*> flags)
{
	/// Get the nth positional argument if it exists

	int positional = 0;
	for (int i = 0; i < argc; i++) {
		if (strlen(argv[i]) >= 1) {
			if (argv[i][0] == prefix) {
				bool is_flag = false;
				for (auto& flag : flags) {
					if (strcmp(argv[i], flag) == 0)
						is_flag = true;
				}
				if (!is_flag)
					i++;
				continue;
			}
			else if (positional == n) {
				if constexpr (std::is_same_v<T, int>)
					return { i, std::atoi(argv[i]) };
				else if constexpr (std::is_same_v<T, long>)
					return { i, std::atol(argv[i]) };
				else if constexpr (std::is_same_v<T, long long>)
					return { i, std::atoll(argv[i]) };
				else
					return { i, argv[i] };
			}
			else
				positional++;
		}
	}

	return { argc, { } };
}

template <typename... Args>
inline constexpr int get_flag(int argc, char const **argv, char const *argname, Args... argnames)
{
	/// Count the number of flags

	auto res = std::count_if(argv, argv + argc, [&](auto& arg) { return strcmp(arg, argname) == 0; });
	if constexpr (sizeof...(Args))
		res += get_flag(argc, argv, argnames...);
	return res;
}

};
